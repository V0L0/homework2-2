//
//  ViewController.swift
//  G70L2blok2
//
//  Created by Volodymyr on 4/11/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit
 
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // Do any additional setup after loading the view.
        
        interestOnDeposit(2019)
        scholarship(scholatshipInMounth:700, costsNumb:1000, inflationNumb:3)
        print("Всего за учебный год понадобится грн. - \(scholarship(scholatshipInMounth: 600, costsNumb: 1100, inflationNumb: 2))")
        zadachyaIII(accumulation: 3000, scholarshipInMounht: 750)
        inversion(someNumber: 731)
    }
    /* Задача 1. Остров Манхэттен был приобретен поселенцами за $24 в 1826 г. Каково было бы в настоящее время состояние их счета, если бы эти 24 доллара были помещены тогда в банк под 6% годового дохода? */
    func interestOnDeposit(_ year: Int) -> Double {
        let percentInYear: Double = 6
        let buyYear = 1826
        let ourDays = year
        let startPrice: Double = 24
        var bodyPlusPercent: Double = startPrice / 100 * percentInYear + startPrice
        
        print("Состояние их счета в \(buyYear) году составляет: \(bodyPlusPercent) $")
        for i in buyYear-2...ourDays {
            bodyPlusPercent = bodyPlusPercent / 100 * 6 + bodyPlusPercent
            
            print("Состояние их счета в \(i) году составляет: \(bodyPlusPercent) $")
        }
        return bodyPlusPercent
    }
    /* Задача 2. Ежемесячная стипендия студента составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
     */
    func scholarship(scholatshipInMounth: Int, costsNumb: Int, inflationNumb: Int) ->   Int {
        let scholatshipInMounth = scholatshipInMounth
        let costsNumb = costsNumb
        let inflationNumb = inflationNumb
        let academicYear = 10
        var costsInflation = 0
        costsInflation = costsNumb / 100 * inflationNumb + costsNumb
        print("В 1-й месяц нужно к степендии добавить \(costsInflation - scholatshipInMounth) грн")
        var needMoney = 0
        var allNeedMoney = 0
        for i in 2...academicYear {
            costsInflation = costsInflation / 100 * inflationNumb + costsInflation
            needMoney = costsInflation - scholatshipInMounth
            print("В \(i)-й месяц нужно к степендии добавить \(needMoney) грн.")
            allNeedMoney += needMoney
        }
        return allNeedMoney
    }
    /* Задача 3. У студента имеются накопления 2400 грн. Ежемесячная стипендия составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию.
      */
    func zadachyaIII(accumulation: Int, scholarshipInMounht: Int) {
        let accumulation = accumulation
        let scholarshipInMounht = scholarshipInMounht
        let costsNumb = 1000
        let pricesUpIn = 3
        var costsInflation = 0
        var breakPiggiBank = accumulation
        costsInflation = costsNumb / 100 * pricesUpIn + costsNumb
        var needMoney = 0
        for i in 1...10 {
            costsInflation = costsNumb / 100 * pricesUpIn + costsNumb
            needMoney = costsInflation - scholarshipInMounht
            breakPiggiBank -= needMoney
            print("На \(i) месяц останется денег \(breakPiggiBank) в копилке")
            if breakPiggiBank < needMoney {
                print("\n Осталось накоплений: \(breakPiggiBank) + степендия \(scholarshipInMounht)")
                print(" Этого явно мало!!!")
                break
            }
        }
    }
    /* Задача 4. 2хзначную цело численную переменную типа 25, 41, 12. После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед 52, 14, 21
      */
    func inversion(someNumber: Int) -> Int {
        let someNumber = someNumber
        var endNumber = 0
        print("Некоторое число       - \(someNumber)")
        if someNumber <= 99 {
            var a = someNumber / 10
            var b = someNumber % 10
            endNumber = b * 10 + a
            print("Инвертированное число - \(endNumber)")
        }
        if 100 <= someNumber && someNumber <= 999 {
            var a = someNumber / 100
            var b = (someNumber - a * 100) / 10
            var c = (someNumber - a * 100) % 10
            endNumber = c * 100 + b * 10 + a
            print("Инвертированное число - \(endNumber)")
        }
        return endNumber
    }
    
}
